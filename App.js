import AppNavBar from './components/AppNavBar';


// Acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register'

// implement page routing in our app
// acquire the utilities from react-router-dom
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
// BrowserRouter -> this is a standard library component for routing in react. This enables navigation amongst views of various components. It will serve as the 'parent' component that will be used to store all the other components.
// as -> alias keyword in JS.

// Routes => it's a new component introduced in V6 of react-router-dom which task is to allow switching between locations.

// JSX Component -> self closing tag
// syntax: <Element />

import './App.css';

function App() {
  return (
    <div>
      <Router>
        <AppNavBar />
        <Routes>  
          <Route path='/' element={<Home />}  />         
          <Route path='/register' element={<Register />}  />
        </Routes>        
      </Router> 
    </div>
  );
}

export default App;
