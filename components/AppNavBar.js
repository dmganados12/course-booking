// Identify which components are needed to build the navigation
import { Navbar, Nav, Container } from 'react-bootstrap'

// Implement Links in the navbar
import {Link} from 'react-router-dom';

// We will now describe how we want our Navbar to look.
function AppNavBar() {
	return(
	<Navbar bg="primary" expand="lg">
		<Container>
			<Navbar.Brand>B156 Booking App</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse>
				<Nav className="ml-auto">
					<Link to="/" className="nav-link"> Home 
					</Link>
					<Link to="/register" className="nav-link">Register</Link>
					
				</Nav>
			</Navbar.Collapse>
		</Container>			
	</Navbar>
	);
};

export default AppNavBar;